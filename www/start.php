<?php

include("config.php");

// si check en cours, proposer de l'arrêter, refuser d'en démarrer
// check en cours : ps aux | grep rtest.sh | grep -v grep | wc -l          != 0

if ($_GET["action"] == "stop"){
    $pid = exec("ps aux | grep rtest.sh | grep -v grep | grep -v 'vi rtest.sh' | awk '{print $2}'");
    if ($pid != ""){
        exec("kill -9 `ps aux | grep rtest.sh | grep -v grep | grep -v 'vi rtest.sh' | awk '{print $2}'`");
        echo "I killed process $pid";
    }
    else{
        echo "Nothing to kill.";
    }
    echo "<br/><br/>Would you like to <a href='".$base_url."start.php'>START A PROCESS</a> ?";
}
else{

    $date_now = date('dmY\.his');

    $nbli = exec("ps aux | grep rtest.sh | grep -v grep | grep -v 'vi rtest.sh' | wc -l");
    if ($nbli === "0"){
        echo shell_exec("nohup bash $rtest_location -p $package_to_test -r \"$package_to_test_repo\" -l $rlib_location  -m $email_addr -u \"".$base_url."view.php?check=check_".$package_to_test."_".$date_now."\" -x $xvfbrun_location  -n $nb_threads -d ".$all_results_dir."/check_".$package_to_test."_".$date_now." > ".$all_results_dir.$date_now.".log 2>&1 & echo $!");
        echo "Process started<br/><br/>";
    echo "nohup bash $rtest_location -p $package_to_test -r \"$package_to_test_repo\" -l $rlib_location  -m $email_addr -u \"".$base_url."view.php?check=check_".$package_to_test."_".$date_now."\" -x $xvfbrun_location  -n $nb_threads -d ".$all_results_dir."check_".$package_to_test."_".$date_now." > ".$all_results_dir.$date_now.".log 2>&1 & echo $! <br/><br/>";
        echo " <a href='".$base_url."view.php?check=check_".$package_to_test."_$date_now'>VIEW REAL TIME RESULTS</a><br/><br/>";
        echo " <a href='".$base_url."start.php?action=stop'>KILL THE PROCESS</a>";
    }
    else{
        echo "PROCESS ALREADY RUNNING. <br/><br/><a href='".$base_url."start.php?action=stop'>KILL THE PROCESS</a><br/>";
    }

}


?>
