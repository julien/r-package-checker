<?php

// name of the package to test
$package_to_test = "mypkg";

// path to rtest bash script
$rtest_location = "/home/user/rcheck/rtest.sh";

// where you want to install packages
$rlib_location = "/home/user/Rlib_test";

//$email_addr = "Emmanuel.Paradis@univ-montp2.fr";
$email_addr = "your@email.address";

// path to xvfb-run
$xvfbrun_location = "/home/user/xvfb-run";

// directory for all results directories
// (where to put check results)
$all_results_dir = "/home/user/rcheck_results/";

// repository to use to get last version of the package to test
$package_to_test_repo = "http://my.pkg.repo.org/";

// number of threads used for parallel checking
$nb_threads = 4;

// base URL where you host the web interface
// (will prefix start.php and view.php in links)
$base_url = "http://my.server.org/r-package-checker/";

?>
