<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type">
<style type="text/css">
	tr { cursor: pointer; }
</style>
</head>
<body>
<?php
include("config.php");

if (isset($_GET["check"])){
    echo "<table width='100%'>";

    $dir = $_GET["check"];


    $nbli = exec("ps aux | grep rtest.sh | grep -v grep | grep -v 'vi rtest.sh' | grep \"$dir\" |  wc -l");
    //$nbli = exec("ps aux | grep rtest.sh | grep $dir | wc -l");
    if ($nbli === "0"){
        echo "Process is NOT RUNNING<br/>";
    }
    else{
        echo "Process is RUNNING<br/>";
    }

    // Display bash script log
    $bash_log = str_replace("check_".$package_to_test."_","",$dir).".log";
    if (file_exists($all_results_dir.$bash_log)){
        $color = "cyan";
        echo "<tr style='background-color:$color' class='title'><td>Bash script log</td>";
        echo "<td></td></tr>";
        echo "<tr class='retractable' id='bashlog'><td colspan=3><textarea cols='240' rows='30'>".file_get_contents($all_results_dir.$bash_log)."</textarea></td></tr>";
    }
    // Display package install log
    //echo $all_results_dir.$dir."/install_pkg.Rout";
    if (file_exists($all_results_dir.$dir."/install_pkg.Rout")){
            if (file_exists($all_results_dir.$dir."/install_pkg_ok")){
                $version = file_get_contents($all_results_dir.$dir."/pkg.version");
                $status = "ok, version $version";
                $color = "#4EE877";
            }
            else if(file_exists($all_results_dir.$dir."/install_pkg_problem")){
                $status = "problem";
                $color = "red";
            }
            else{
                $status = "running";
                $color = "yellow";
            }
        echo "<tr style='background-color:$color' class='title'><td>$package_to_test INSTALL PROCESS</td>";
        echo "<td>$status</td></tr>";
        echo "<tr class='retractable' id='install'><td colspan=3><textarea cols='240' rows='30'>".file_get_contents($all_results_dir.$dir."/install_pkg.Rout")."</textarea></td></tr>";
    }

    // Display R packages update log
    //echo $all_results_dir.$dir."/update.Rout";
    if (file_exists($all_results_dir.$dir."/update.Rout")){
            if (file_exists($all_results_dir.$dir."/update_ok")){
                $status = "ok";
                $color = "#4EE877";
            }
            else if(file_exists($all_results_dir.$dir."/update_problem")){
                $status = "problem";
                $color = "red";
            }
            else{
                $status = "running";
                $color = "yellow";
            }
        echo "<tr style='background-color:$color' class='title'><td>UPDATE PROCESS</td>";
        echo "<td>$status</td></tr>";
        echo "<tr class='retractable' id='update'><td colspan=3><textarea cols='200' rows='30'>".file_get_contents($all_results_dir.$dir."/update.Rout")."</textarea></td></tr>";
    }

    // install logs of all packages depending on PACKAGE_TO_TEST

    foreach(scandir($all_results_dir.$dir) as $elem){
        // si c'est un package tar
        if (strpos($elem,"_install.Rout") != False){

            $package = explode("_",$elem);
            $package_name = $package[0];

            // si CHECKOK
            if (file_exists($all_results_dir.$dir."/".$package_name."_install_ok")){
                $status = "ok";
                $color = "#4EE877";
            }
            // si CHECKPROBLEM
            else if(file_exists($all_results_dir.$dir."/".$package_name."_install_problem")){
                $status = "problem";
                $color = "red";
            }
            else{
                $status = "running";
                $color = "yellow";
            }

            $log = file_get_contents($all_results_dir.$dir."/".$package_name."_install.Rout");
            echo "<tr style='background-color:$color' class='title'><td>INSTALL $package_name</td>";
            echo "<td>$status</td></tr>";
            echo "<tr class='retractable' id='$package_name'><td colspan=3><textarea cols='200' rows='30'>$log</textarea></td></tr>";
        }
    }

    // check logs of all packages depending on PACKAGE_TO_TEST

    foreach(scandir($all_results_dir.$dir) as $elem){
        // si c'est un package tar
        if (strpos($elem,".tar.gz") != False){

            $package = explode("_",str_replace(".tar.gz","",$elem));
            $package_name = $package[0];
            $package_version = $package[1];

            // si CHECKOK
            if (file_exists($all_results_dir.$dir."/".$package_name.".CHECKOK")){
                $status = "ok";
                $color = "#4EE877";
            }
            // si CHECKPROBLEM
            else if(file_exists($all_results_dir.$dir."/".$package_name.".CHECKPROBLEM")){
                $status = "problem";
                $color = "red";
            }
            else{
                $status = "running";
                $color = "yellow";
            }

            $log = file_get_contents($all_results_dir.$dir."/".$package_name.".Rcheck/00check.log");
            echo "<tr style='background-color:$color' class='title'><td>CHECK $package_name V $package_version</td>";
            echo "<td>$status</td></tr>";
            echo "<tr class='retractable' id='$package_name'><td colspan=3><textarea cols='200' rows='30'>$log</textarea></td></tr>";
        }
    }
    echo "</table>";
}
else{
    # show all available checks
    foreach(scandir($all_results_dir) as $elem){
        // si c'est un dossier de check
        if (strpos("a".$elem,"check_") != False){
            echo "<a href='?check=$elem'>$elem</a><br/>";
        }
    }
}
?>
<script type="text/javascript" src="jquery-1.7.2.min.js"></script>
<script>
$( document ).ready(function() {

	jQuery.fn.putCursorAtEnd = function() {

		return this.each(function() {

			$(this).focus()

				// If this function exists...
				if (this.setSelectionRange) {
					// ... then use it (Doesn't work in IE)

					// Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
					var len = $(this).val().length * 2;

					this.setSelectionRange(len, len);

				} else {
					// ... otherwise replace the contents with itself
					// (Doesn't work in Google Chrome)

					$(this).val($(this).val());

				}

			// Scroll to the bottom, in case we're in a tall textarea
			// (Necessary for Firefox and Google Chrome)
			this.scrollTop = 999999;

		});

	};



    $('.retractable').each(function(){
        $(this).hide();
    });
    $('.title').each(function(){
        $(this).click(function(){
            if (! $(this).next().is(":visible")){
                $(this).next().show();
		$(this).next().find('textarea').putCursorAtEnd();
            }
            else{
                $(this).next().hide();
            }
        });
    });
    $("html, body").animate({ scrollTop: $(document).height() }, "slow");
});
</script>

</body>
</html>
