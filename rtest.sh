#!/bin/bash

#DEFAULT_REPOSITORY="http://cran.irsn.fr/"
DEFAULT_REPOSITORY="http://cran.r-project.org/"
XVFBRUN_PATH=xvfb-run
if [ -z ${JAVA_HOME+x} ] || ! [ -d $JAVA_HOME ]; then
    export JAVA_HOME="/usr/lib/jvm/java-7-openjdk-amd64/jre"
fi
export PATH=$PATH:$JAVA_HOME/bin

# function that takes a destination directory as argument and produces
# an HTML report as index.html into it
function generate_html_report(){
    local dir=$1
    local dest_html=$1/index.html
    echo "<html>
<head>
<meta content='text/html; charset=utf-8' http-equiv='content-type'>
</head>
<body>
<table width='100%'>
" > $dest_html

    # show install log of package to test
    if [ -f $dir"/install_pkg.Rout" ]; then
            if [ -f $dir"/install_pkg_ok" ]; then
                local version=`cat $dir"/pkg.version"`
                local status="ok, version $version"
                local color="#4EE877"
            else 
                if [ -f $dir"/install_pkg_problem" ]; then
                    local status="problem"
                    local color="red"
                else
                    local status="running"
                    local color="yellow"
                fi
            fi
        echo "<tr style='background-color:$color' class='title'><td>$PACKAGE_TO_TEST INSTALL PROCESS</td>" >> $dest_html
        echo "<td>$status</td></tr>" >> $dest_html
        echo "<tr class='retractable' id='update'><td colspan=3><textarea cols='240' rows='30'>`cat $dir/install_pkg.Rout`</textarea></td></tr>" >> $dest_html
    fi

    # show update log
    if [ -f $dir/update.Rout ]; then
        if [ -f $dir/update_ok ]; then
            local status="ok"
            local color="#4EE877"
        else 
            if [ -f $dir/update_problem ]; then
                local status="problem"
                local color="red"
            else
                local status="running"
                local color="yellow"
            fi
        fi
        local nlines=`cat $dir/update.Rout | wc -l`
        echo "<tr style='background-color:$color' class='title'><td>UPDATE PROCESS</td>" >> $dest_html
        echo "<td>$status ($nlines lines)</td></tr>" >> $dest_html
        echo "<tr class='retractable' id='update'><td colspan=3><textarea cols='200' rows='30'>`cat $dir/update.Rout`</textarea></td></tr>" >> $dest_html
    fi

    # INSTALL, for all the install R scripts output files
    if [[ "`ls $dir | grep "_install.Rout" | wc -l`" != "0" ]]; then
        for elem in `ls $dir/*_install.Rout`; do
            local package=`basename $elem`
            local package_name=${package/_install.Rout/}

            # if the installation went well
            if [ -f $dir"/"$package_name"_install_ok" ]; then
                local status="ok"
                local color="#4EE877"
            # if the installation failed
            else 
                if [ -f $dir"/"$package_name"_install_problem" ]; then
                    local status="problem"
                    local color="red"
                else
                    local status="running"
                    local color="yellow"
                fi
            fi

            local log=`cat $dir"/"$package_name"_install.Rout"`
            local nlines=`cat $dir"/"$package_name"_install.Rout" | wc -l`
            echo "<tr style='background-color:$color' class='title'><td>INSTALL $package_name</td>" >> $dest_html
            echo "<td>$status ($nlines lines)</td></tr>" >> $dest_html
            echo "<tr class='retractable' id='$package_name'><td colspan=3><textarea cols='200' rows='30'>$log</textarea></td></tr>" >> $dest_html
        done
    fi

    # CHECK, for all the downloaded archives
    if [[ "`ls $dir | grep ".tar.gz" | wc -l`" != "0" ]]; then
        for elem in `ls $dir/*.tar.gz`; do

            local baselem=`basename $elem`
            local baselem=${baselem/.tar.gz/}
            # split string into array
            local package=(`echo "$baselem" | tr "_" "\n"`)
            #IFS='_' read -ra package <<< "$baselem"
            local package_name=${package[0]}
            local package_version=${package[1]}

            if [ -d $dir"/"$package_name".Rcheck/" ]; then
                # si CHECKOK
                if [ -f $dir"/"$package_name".CHECKOK" ]; then
                    local status="ok"
                    local color="#4EE877"
                # si CHECKPROBLEM
                else 
                    if [ -f $dir"/"$package_name".CHECKPROBLEM" ]; then
                        local status="problem"
                        local color="red"
                    else
                        local status="running"
                        local color="yellow"
                    fi
                fi

                local log=`cat $dir"/"$package_name".Rcheck/00check.log"`
                local nlines=`cat $dir"/"$package_name".Rcheck/00check.log" | wc -l`
                echo "<tr style='background-color:$color' class='title'><td>CHECK $package_name V $package_version</td>" >> $dest_html
                echo "<td>$status ($nlines lines)</td></tr>" >> $dest_html
                echo "<tr class='retractable' id='$package_name'><td colspan=3><textarea cols='200' rows='30'>$log</textarea></td></tr>" >> $dest_html
            fi
        done
    fi

    echo "</table>" >> $dest_html
    echo '
<script type="text/javascript" src="jquery-1.7.2.min.js"></script>
<script>
$( document ).ready(function() {
    $(".retractable").each(function(){
        $(this).hide();
    });
    $(".title").each(function(){
        $(this).click(function(){
            if (! $(this).next().is(":visible")){
                $(this).next().show();
            }
            else{
                $(this).next().hide();
            }
        });
    });
    $("tr").hover(function() {
        $(this).css("cursor","pointer");
    });

});
</script>

</body>
</html>
' >> $dest_html
}

############### PRINTING FUNCTIONS ###################

txtred=$(tput setaf 1) # Red
txtgrn=$(tput setaf 2) # Green
txtpur=$(tput setaf 5) # Purple
txtrst=$(tput sgr0) # Text reset.

# all of those functions avoid printing colors and right aligned text if the
# output ou rtest was redirected to a file or a pipe

# print left in purple and does NOT put the end-of-line char
function printLeftPurple(){
    if [[ -t 1 ]]; then
        columns=$(tput cols)
        col1="100"
        printf "${txtpur}%-*s" "$col1" "$1"
    else
        printf "%-*s" "$col1" "$1"
    fi
    export LAST_LENGTH=${#1}
}

# print right in green and DOES put the end-of-line char
function printRightGreen(){
    if [[ -t 1 ]]; then
        columns=$(tput cols)
        col1="100"
        let "col2 = $columns - $col1"
        printf "${txtgrn}%*s${txtrst}\n" "$col2" "$1"
    else
        let "place = 120 - $LAST_LENGTH"
        printf "%*s\n" "$place" "$1"
    fi
}

# print right in red and DOES put the end-of-line char
function printRightRed(){
    if [[ -t 1 ]]; then
        columns=$(tput cols)
        col1="100"
        let "col2 = $columns - $col1"
        printf "${txtred}%*s${txtrst}\n" "$col2" "$1"
    else
        let "place = 120 - $LAST_LENGTH"
        printf "%*s\n" "$place" "$1"
    fi
}

function printUsage(){
echo "`basename $0` is a tool to test that a R package (PACKAGE_TO_TEST) does not break packages that depend on it by running 'R CMD check' for each of them.

usage :
$0 [-p|--package NAME_OF_PACKAGE_TO_TEST || -c|--checklist CHECKLIST_FILE || --checklistraw -t|--tarfolder TAR_FOLDER_PATH] [-l|--libuser R_USER_LIB_VALUE] [-d|--dest DESTINATION_DIRECTORY] [-m|--mail MAIL_TO] [-u|--urlresult URL] [-r|--repo PACKAGE_TO_TEST_REPO] [-x|--xvfb XVFB-RUN PATH] [-n|--nbcores NBCORES] [-s|--skipinstall] [-h|--html] [--rpath PATH_TO_R_BINARY]

-p | --package NAME_OF_PACKAGE_TO_TEST : name of package to test (case sensible), all packages depending on this package are going to be checked by their R CMD CHECK
-c | --checklist CHECKLIST_FILE : file listing packages you want to check (each line should look like : PKG_NAME,PKG_VERSION,anything)
--checklistraw RAW_CHECKLIST_FILE : file listing packages you want to check (simply one package name per line)
-t | --tarfolder TAR_FOLDER_PATH : path to folder where tarballs of packages can be found. All of these packages are going to be \"R CMD CHECKED\"
-r | --repo PACKAGE_TO_TEST_REPO : repository of package you want to test (DEFAULT : R-CRAN)
-l | --libuser R_LIBS_USER : directory where packages depending on PACKAGE_TO_TEST are going to be installed (DEFAULT : R will choose it)
                             This directory can be used several times for several tests. Using same directory several times will save time.
                             This directory can exist or not. If not, it will be created
-d | --dest DESTINATION_DIRECTORY : result directory where HTML result and test files are produced (DEFAULT : ./check_PKGNAME_DATE)
-m | --mail MAIL_TO : email address for real time user information (DEFAULT : NO MAIL WILL BE SENT)
-u | --urlresult URL : if -m option was set, this http link to test results will be sent instead of a file path (DEFAULT : destination directory absolute path)
-x | --xvfb PATH : Specific path to xvfb-run script (DEFAULT : 'xvfb-run')
-n | --nbcores NBCORES : Number of cores for parallel executions of installations (Ncpu parameter) and number of simultaneous checks (DEFAULT : 1)
-s | --skipinstall : flag to skip the packages update/installation
-h | --html : generate html report in DESTINATION_DIRECTORY/index.html . Will download jquery in destination directory
--rpath PATH_TO_R_BINARY : will use PATH_TO_R_BINARY to call R (default=R)

Don't forget to set JAVA_HOME if some dependencies need java, which is quite probable.

"
}

ARGS=$(getopt -o t:c:p:r:d:m:l:x:n:shu: --long rpath:,tarfolder:,checklist:,checklistraw:,package:,repo:,mail:,libuser:,dest:,xvfb:,nbcores:,skipinstall,html,urlresult: -n "$0" -- "$@");

#Bad arguments
if [ $? -ne 0 ] || [ $# -eq 0 ];
then
    printUsage
    exit
fi
REPOSITORYFLAG=0
MAILTOFLAG=0
DESTINATIONFLAG=0
PACKAGE_TO_TEST=""
CHECKLIST_FILE=""
TAR_FOLDER_PATH=""
NBCORES=1
SKIPINSTALLFLAG=0
HTMLFLAG=0
RESULTURLFLAG=0
R_LIBS_USER_FLAG=0
RPATH=R
eval set -- "$ARGS";

while true; do
    case "$1" in
        -d|--dest)
            shift;
            if [ -n "$1" ]; then
                if [ -d "$1" ] || [ -f "$1" ]; then
                    echo "File or directory $1 already exists

                    "
                    printUsage
                    exit
                else
                    DESTINATIONFLAG=1
                    DESTINATION=$(readlink -f "$1")
                fi
            else
                echo "You have to set a destination directory

                "
                printUsage
                exit
            fi
            shift;
            ;;
        --rpath)
            shift;
            if [ -n "$1" ]; then
                RPATH=$1
            else
                echo "You have to set a non-empty R PATH

                "
                printUsage
                exit
            fi
            shift;
            ;;
        -l|--libuser)
            shift;
            if [ -n "$1" ]; then
                if ! [ -d "$1" ]; then
                    mkdir "$1"
                    echo "CREATING '$1' personnal R library"
                fi
                export R_LIBS_USER=$(readlink -f "$1")
                R_LIBS_USER_FLAG=1
            else
                echo "You have to set a non-empty R LIB USER directory

                "
                printUsage
                exit
            fi
            shift;
            ;;
        -r|--repo)
            shift;
            if [ -n "$1" ]; then
                REPOSITORYFLAG=1
                REPOSITORY=$1
            else
                echo "You have to set a non-empty repository

                "
                printUsage
                exit
            fi
            shift;
            ;;
        -m|--mail)
            shift;
            if [ -n "$1" ]; then
                MAILTOFLAG=1
                MAILTO=$1
            else
                echo "You have to set a non-empty destination EMAIL ADDRESS

                "
                printUsage
                exit
            fi
            shift;
            ;;
        -u|--urlresult)
            shift;
            if [ -n "$1" ]; then
                RESULTURLFLAG=1
                RESULT_URL=$1
            else
                echo "You have to set a non-empty result url

                "
                printUsage
                exit
            fi
            shift;
            ;;
        -p|--package)
            shift;
            if [ -n "$1" ]; then
                PACKAGE_TO_TEST=$1
                MAIL_SUBJECT_BEGIN="Package \"$PACKAGE_TO_TEST\""
            else
                echo "You have to set a non-empty PACKAGE_TO_TEST

                "
                printUsage
                exit
            fi
            shift;
            ;;
        -c|--checklist)
            shift;
            if [ -n "$1" ]; then
                CHECKLIST_FILE="$1"
                MAIL_SUBJECT_BEGIN="List of packages \"$CHECKLIST_FILE\""
                if ! [ -f $CHECKLIST_FILE ]; then
                    echo "List file $CHECKLIST_FILE not found...

                    "
                    exit
                fi
            else
                echo "You have to set a non-empty CHECKLIST_FILE path

                "
                printUsage
                exit
            fi
            shift;
            ;;
        --checklistraw)
            shift;
            if [ -n "$1" ]; then
                CHECKLIST_RAW_FILE="$1"
                MAIL_SUBJECT_BEGIN="List of packages \"$CHECKLIST_RAW_FILE\""
                if ! [ -f $CHECKLIST_RAW_FILE ]; then
                    echo "List file $CHECKLIST_RAW_FILE not found...

                    "
                    exit
                else
                    echo '
mytemp <- tempfile();
download.file("'$DEFAULT_REPOSITORY'web/packages/packages.rds", mytemp);
mydata <- as.data.frame(readRDS(mytemp), row.names=NA);
mydata$Published <- as.Date(mydata[["Published"]]);

#sort and get the fields you like:
a <- mydata[order(mydata$Package),c("Package", "Version", "Published")];
ff <- file("/tmp/Rversions.txt", open = "w")
cat( paste(sep=",",a[,1],sub("\n","",a[,2]),sub("\n"," ",a[,3]), collapse = "\n"), "\n", file=ff)
close(ff)
                    ' > /tmp/getVersions.R
                    $RPATH CMD BATCH /tmp/getVersions.R > /dev/null 2>&1
                    cat /tmp/Rversions.txt | sort -t , -k 1,1 > /tmp/join1
                    cat $CHECKLIST_RAW_FILE | sort -t , -k 1,1 > /tmp/join2
                    join /tmp/join1 /tmp/join2 -j 1 -t ',' > /tmp/list_with_versions.txt
                    CHECKLIST_FILE="/tmp/list_with_versions.txt"
                fi
            else
                echo "You have to set a non-empty CHECKLIST_RAW_FILE path

                "
                printUsage
                exit
            fi
            shift;
            ;;
        -t|--tarfolder)
            shift;
            if [ -n "$1" ]; then
                TAR_FOLDER_PATH="$1"
                MAIL_SUBJECT_BEGIN="Folder of packages \"$TAR_FOLDER_PATH\""
                if ! [ -d $TAR_FOLDER_PATH ]; then
                    echo "Folder $TAR_FOLDER_PATH not found...

                    "
                    exit
                fi
            else
                echo "You have to set a non-empty TAR_FOLDER_PATH path

                "
                printUsage
                exit
            fi
            shift;
            ;;
        -x|--xvfb)
            shift;
            if [ -n "$1" ]; then
                XVFBRUN_PATH="$1"
                if ! [ -f $XVFBRUN_PATH ]; then
                    echo "xvfb-run not found...

                    "
                    exit
                fi
            else
                echo "You have to set a non-empty XVFB-RUN path

                "
                printUsage
                exit
            fi
            shift;
            ;;
        -n|--nbcores)
            shift;
            if [ -n "$1" ]; then
                NBCORES="$1"
                re='^[0-9]+$'
                if ! [[ $NBCORES =~ $re ]] ; then
                       echo "-n --nbcores parameter must be a number" >&2; exit 1
                fi
            else
                echo "You have to set a non-empty nbcores parameter

                "
                printUsage
                exit
            fi
            shift;
            ;;
        -s|--skipinstall)
            SKIPINSTALLFLAG=1
            shift;
            ;;
        -h|--html)
            HTMLFLAG=1
            shift;
            ;;
        --)
            break;
            ;;
    esac
done

## getting non-option arguments : THE MAIN ONE : PACKAGE_TO_TEST !
## only last non-option argument will be taken
#shift;
#while [ -n "$1" ]; do
#    PACKAGE_TO_TEST=$1
#    shift
#done

if [ $DESTINATIONFLAG == 1 ]; then
    RVERSION=`$RPATH --version | head -n 1 | awk '{print $3}'`
    mkdir "$R_LIBS_USER/$RVERSION"
    export R_LIBS_USER=$R_LIBS_USER/$RVERSION
fi

if ! [ -n "$PACKAGE_TO_TEST" ]; then
    if ! [ -n "$CHECKLIST_FILE" ]; then
        if ! [ -n "$TAR_FOLDER_PATH" ]; then
            echo "
There must be a package to test OR a CHECKLIST_FILE OR a TAR_FOLDER_PATH.

At least -p or -c or -t option must be set.

"
            exit
        fi
    fi
fi

if ([ -n "$CHECKLIST_FILE" ] && [ -n "$TAR_FOLDER_PATH" ]) ||
   ([ -n "$CHECKLIST_FILE" ] && [ -n "$PACKAGE_TO_TEST" ]) ||
   ([ -n "$PACKAGE_TO_TEST" ] && [ -n "$TAR_FOLDER_PATH" ]); then
    echo "
There must choose between testing a specific package (R CMD CHECK on all packages depending on it) OR checking a given list of packages.

In other words : choose between -p and -c and -t option.
"
    exit
fi

#### DEFAULT VALUES management

# Default destination directory is ./check_PKGNAME_DATE
if [ $DESTINATIONFLAG == 0 ]; then
    DESTINATION=$(readlink -f "./check_${PACKAGE_TO_TEST}_`date '+%Y%m%d.%H%M%S'`")
fi
if [ $RESULTURLFLAG == 0 ]; then
    RESULT_URL=$DESTINATION
fi

# RECAP OF ALL PARAMS
if [ -n "$PACKAGE_TO_TEST" ]; then
    echo "PACKAGE TO TEST : $PACKAGE_TO_TEST . Last version of $PACKAGE_TO_TEST and all packages depending on $PACKAGE_TO_TEST will be installed and checked"
else
    if [ -n "$CHECKLIST_FILE" ]; then
        echo "LIST OF PACKAGES TO INSTALL AND CHECK : $CHECKLIST_FILE"
    else
        echo "FOLDER OF PACKAGES TO INSTALL AND CHECK : $TAR_FOLDER_PATH"
    fi
fi
if [ -n "$R_LIBS_USER" ]; then
    echo "R_LIBS_USER : $R_LIBS_USER"
else
    echo "R_LIBS_USER : EMPTY => DEFAULT"
fi
echo "DESTINATION DIRECTORY : $DESTINATION"
if [[ $MAILTOFLAG == 1 ]]; then
    echo "MAIL will be sent to $MAILTO"
else
    echo "NO MAILS"
fi
if [[ $RESULTURLFLAG == 1 ]]; then
    echo "Result URL is $RESULT_URL"
fi
if [[ $REPOSITORYFLAG == 1 ]]; then
    echo "Package REPOSITORY : $REPOSITORY"
else
    echo "NO SPECIFIC PACKAGE REPOSITORY"
fi
if [[ $SKIPINSTALLFLAG == 1 ]]; then
    echo "Installation of packages will be skipped"
fi
if [[ $HTMLFLAG == 1 ]]; then
    echo "HTML report will be generated in $DESTINATION/index.html"
else
    echo "NO HTML report will be generated"
fi
echo "rtest will be using $NBCORES simultaneous processes"


# send start mail
if [[ $MAILTOFLAG == 1 ]]; then
    echo "Check progress and results are here :

$RESULT_URL

Your test directory is \"$DESTINATION\"

" | mail -s "$MAIL_SUBJECT_BEGIN R check started (`basename $DESTINATION`)" $MAILTO
fi

######## FILE management

# Destination directory creation
mkdir $DESTINATION
cd $DESTINATION
if [ -n "$TAR_FOLDER_PATH" ]; then
    for i in $TAR_FOLDER_PATH/*; do
        cp $i ./
    done
fi

if [[ $HTMLFLAG == 1 ]]; then
    echo "Downloading jquery inside result folder..."
    wget http://code.jquery.com/jquery-1.7.2.min.js -O $DESTINATION/jquery-1.7.2.min.js
    echo
fi

# if no package to test but a list : do not generate depends.R and do not install
if [ -n "$PACKAGE_TO_TEST" ]; then
    echo
    printLeftPurple "Generation of depends.R ... started at `date '+%H:%M:%S'` ... "
    # generate depends.R
    echo 'if (!"tools" %in% installed.packages(lib.loc=Sys.getenv("R_LIBS_USER"))) {
        install.packages("tools",repos=c("'$DEFAULT_REPOSITORY'"), lib=Sys.getenv("R_LIBS_USER"), dependencies=c("Depends", "Imports", "LinkingTo", "Suggests"), Ncpus='$NBCORES')
    }

    old <- getOption("defaultPackages"); r <- getOption("repos")
    r["CRAN"] <- "'$DEFAULT_REPOSITORY'"
    options(defaultPackages = c(old, "MASS"), repos = r)
    require("tools")

    reverse_dependencies_with_maintainers <-
    function(packages, which = c("Depends", "Imports", "LinkingTo"),
             recursive = FALSE)
    {
        contrib.url(getOption("repos")["CRAN"], "source") # trigger chooseCRANmirror() if required
        description <- sprintf("%s/web/packages/packages.rds",
                               getOption("repos")["CRAN"])
        con <- if(substring(description, 1L, 7L) == "file://")
            file(description, "rb")
        else
            url(description, "rb")
        on.exit(close(con))
        db <- readRDS(gzcon(con))
        rownames(db) <- NULL

        rdepends <- package_dependencies(packages, db, which,
                                         recursive = recursive,
                                         reverse = TRUE)
        rdepends <- sort(unique(unlist(rdepends)))
        pos <- match(rdepends, db[, "Package"], nomatch = 0L)

        db[pos, c("Package", "Version", "Maintainer")]
    }

    Sys.setenv(http_proxy = "")
    a <- reverse_dependencies_with_maintainers("'$PACKAGE_TO_TEST'", which = c("Depends", "Imports", "Suggests", "LinkingTo"))
    a <- a[order(a[,1]),]
    ff <- file("depends.txt", open = "w")
    cat( paste(sep=",",a[,1],sub("\n","",a[,2]),sub("\n"," ",a[,3]), collapse = "\n"), "\n", file=ff)
    close(ff)
    ' > $DESTINATION/depends.R

    printRightGreen "`date '+%H:%M:%S'` : OK"

    ################ INSTALL of package to test ####################

    printLeftPurple "Installing $PACKAGE_TO_TEST ... started at `date '+%H:%M:%S'` ... "

    REPO_R_PARAM=""
    if [[ $REPOSITORYFLAG == 1 ]]; then
        REPO_R_PARAM=", repos=c(\"$REPOSITORY\")"
    else
        REPO_R_PARAM=", repos=c(\"$DEFAULT_REPOSITORY\")"
    fi
    R_LIBS_USER_PARAM=""
    if [ -n "$R_LIBS_USER" ]; then
        R_LIBS_USER_PARAM=", lib=Sys.getenv(\"R_LIBS_USER\")"
    fi
    echo '
    install.packages("'$PACKAGE_TO_TEST'"'$REPO_R_PARAM''$R_LIBS_USER_PARAM', dependencies=c("Depends", "Imports", "LinkingTo", "Suggests"), Ncpus='$NBCORES')

    ff <- file("pkg.version", open = "w")
    cat( paste(sep=",",packageVersion("'$PACKAGE_TO_TEST'")), file=ff)
    close(ff)
    ' > $DESTINATION/install_pkg.R
    $RPATH CMD BATCH install_pkg.R

    if [[ $HTMLFLAG == 1 ]]; then
        generate_html_report $DESTINATION
    fi

    if [[ $? == 0 ]]; then
        touch install_pkg_ok
        printRightGreen "`date '+%H:%M:%S'` : OK"
    else
        touch install_pkg_problem
        printRightRed "`date '+%H:%M:%S'` : FAILED"
    fi

    ############### GENERATION OF DEPENDENCY LIST ###################

    printLeftPurple "Generating dependency list (should take less than a minute)... started at `date '+%H:%M:%S'` ... "
    $RPATH CMD BATCH --no-timing depends.R
    #cat depends.txt | grep -v structSSI | grep -v SigTree | grep -v SeqFeatR | grep -v gWidgetsRGtk2 | grep -v GrammR | grep -v RADami > list_dependencies.txt
    cat depends.txt > list_dependencies.txt
    printRightGreen "`date '+%H:%M:%S'` : OK"

# END of big if "there is a package to test"
fi
#################### UPDATE of R packages in R_LIBS_USER #####################

if [[ "$SKIPINSTALLFLAG" == "0" ]]; then
    printLeftPurple "Updating R packages in R_LIBS_USER (takes a very variable time)... started at `date '+%H:%M:%S'` ... "
    echo '
    chooseBioCmirror(ind=3)
    setRepositories(ind=c(1,2,3,4,5,6,7,8,9))
    update.packages(lib.loc=Sys.getenv("R_LIBS_USER"), repos=c("'$DEFAULT_REPOSITORY'"),ask=FALSE)

    if (!"XML" %in% installed.packages(lib.loc=Sys.getenv("R_LIBS_USER"))) {
        install.packages("XML",repos=c("'$DEFAULT_REPOSITORY'"), lib=Sys.getenv("R_LIBS_USER"), dependencies=c("Depends", "Imports", "LinkingTo", "Suggests"), Ncpus='$NBCORES')
    }
    source("http://bioconductor.org/biocLite.R")
    biocLite("Biostrings", ask=FALSE, lib.loc=Sys.getenv("R_LIBS_USER"), suppressUpdates=TRUE)
    biocLite("Heatplus", ask=FALSE, lib.loc=Sys.getenv("R_LIBS_USER"), suppressUpdates=TRUE)
    biocLite("qvalue", ask=FALSE, lib.loc=Sys.getenv("R_LIBS_USER"), suppressUpdates=TRUE)
    biocLite("graph", ask=FALSE, lib.loc=Sys.getenv("R_LIBS_USER"), suppressUpdates=TRUE)
    biocLite("multtest", ask=FALSE, lib.loc=Sys.getenv("R_LIBS_USER"), suppressUpdates=TRUE)
    biocLite("widgetTools", ask=FALSE, lib.loc=Sys.getenv("R_LIBS_USER"), suppressUpdates=TRUE)
    biocLite("phyloseq", ask=FALSE, lib.loc=Sys.getenv("R_LIBS_USER"), suppressUpdates=TRUE)
    biocLite("Rgraphviz", ask=FALSE, lib.loc=Sys.getenv("R_LIBS_USER"), suppressUpdates=TRUE)
    biocLite("supraHex", ask=FALSE, lib.loc=Sys.getenv("R_LIBS_USER"), suppressUpdates=TRUE)
    install.packages("mpoly",lib=Sys.getenv("R_LIBS_USER"),repos="'$DEFAULT_REPOSITORY'")
    ' > $DESTINATION/update.R
    $XVFBRUN_PATH -n 222 $RPATH CMD BATCH --slave --vanilla --quiet update.R

    ret_upd=$?
    non_zero=`grep -i "non-zero exit status" update.Rout | wc -l`

    if [[ $ret_upd == 0 ]] && [[ $non_zero == 0 ]]; then
        touch update_ok
        printRightGreen "`date '+%H:%M:%S'` : OK"
    else
        touch update_problem
        printRightRed "`date '+%H:%M:%S'` : FAILED"
    fi

    if [[ $HTMLFLAG == 1 ]]; then
        generate_html_report $DESTINATION
    fi
fi

################ INSTALL packages if they are not installed in R_LIBS_USER ###################
if [ -n "$PACKAGE_TO_TEST" ]; then
    PACKAGES_TO_CHECK_LIST_FILE="list_dependencies.txt"
else
    if [ -n "$CHECKLIST_FILE" ]; then
        cp $CHECKLIST_FILE ./list_packages_to_check.txt
        PACKAGES_TO_CHECK_LIST_FILE=$CHECKLIST_FILE
    fi
fi

if [[ "$SKIPINSTALLFLAG" == "0" ]]; then
    if [ -n "$PACKAGE_TO_TEST" ] || [ -n "$CHECKLIST_FILE" ]; then
        echo "
Installing `wc -l $PACKAGES_TO_CHECK_LIST_FILE` packages in R_LIBS_USER (may be long if launched for the first time in this R_LIBS_USER)
        "
        while read line; do
            pname=`echo $line | cut -d ',' -f 1`
            pversion=`echo $line | cut -d ',' -f 2`
            printLeftPurple "Installing $pname v $pversion ... started at `date '+%H:%M:%S'` ... "
            # local install
            echo 'chooseBioCmirror(ind=3)
    setRepositories(ind=c(1, 2, 5, 6))
    if (!"'$pname'" %in% installed.packages(lib.loc=Sys.getenv("R_LIBS_USER"))) {
        install.packages("'$pname'",repos=c("'$DEFAULT_REPOSITORY'"), lib=Sys.getenv("R_LIBS_USER"), dependencies=c("Depends", "Imports", "LinkingTo", "Suggests"), Ncpus='$NBCORES')
    }
    ' > ${pname}_install.R

            $XVFBRUN_PATH -n 222 $RPATH CMD BATCH --slave --vanilla --quiet ${pname}_install.R
            # we do it twice to be sure...
            $XVFBRUN_PATH -n 222 $RPATH CMD BATCH --slave --vanilla --quiet ${pname}_install.R
            ret=`grep -i "non-zero exit status|statut de sortie non nul" ${pname}_install.Rout | wc -l`
            if [[ $ret == '0' ]]; then
                touch ${pname}_install_ok
                printRightGreen "`date '+%H:%M:%S'` : OK"
            else
                touch ${pname}_install_problem
                printRightRed "`date '+%H:%M:%S'` : FAILED"
            fi

            if [[ $HTMLFLAG == 1 ]]; then
                generate_html_report $DESTINATION
            fi
        done < $PACKAGES_TO_CHECK_LIST_FILE
    else
        ######## we have a TAR_FOLDER_PATH ###########
        echo "
Installing `ls $TAR_FOLDER_PATH | wc -l` packages in R_LIBS_USER (may be long if launched for the first time in this R_LIBS_USER)
        "
        for i in $TAR_FOLDER_PATH/*; do
            file=`basename $i`
            pname=`echo $file | cut -d '_' -f 1`
            pversion=`echo $file | cut -d '_' -f 2`
            printLeftPurple "Installing $pname v $pversion ... started at `date '+%H:%M:%S'` ... "
            # local install
            echo 'chooseBioCmirror(ind=3)
    setRepositories(ind=c(1, 2, 5, 6))
    if (!"'$pname'" %in% installed.packages(lib.loc=Sys.getenv("R_LIBS_USER"))) {
        install.packages("'$pname'",repos=c("'$DEFAULT_REPOSITORY'"), lib=Sys.getenv("R_LIBS_USER"), dependencies=c("Depends", "Imports", "LinkingTo", "Suggests"), Ncpus='$NBCORES')
    }
    ' > ${pname}_install.R

            $XVFBRUN_PATH -n 222 $RPATH CMD INSTALL -l "$R_LIBS_USER" $file > ${pname}_install.Rout 2>&1
            # we do it twice to be sure...
            $XVFBRUN_PATH -n 222 $RPATH CMD INSTALL -l "$R_LIBS_USER" $file > ${pname}_install.Rout 2>&1
            ret=`grep -i "non-zero exit status|statut de sortie non nul" ${pname}_install.Rout | wc -l`
            if [[ $ret == '0' ]]; then
                touch ${pname}_install_ok
                printRightGreen "`date '+%H:%M:%S'` : OK"
            else
                touch ${pname}_install_problem
                printRightRed "`date '+%H:%M:%S'` : FAILED"
            fi

            if [[ $HTMLFLAG == 1 ]]; then
                generate_html_report $DESTINATION
            fi
        done
    fi
fi

###################### R CMD check #############################

echo "
Checking packages with 'R CMD check'
"
# we don't verify that suggested packages are installed
export _R_CHECK_FORCE_SUGGESTS_=FALSE

# SIMPLE CORE PROCESSING

if [[ $NBCORES == 1 ]]; then
    if [ -n "$PACKAGE_TO_TEST" ] || [ -n "$CHECKLIST_FILE" ]; then
        while read line; do
            pname=`echo $line | cut -d ',' -f 1`
            pversion=`echo $line | cut -d ',' -f 2`
            printLeftPurple "R CMD check ${pname}_${pversion}.tar.gz ... started at `date '+%H:%M:%S'` ... "

            wget $DEFAULT_REPOSITORY/src/contrib/${pname}_${pversion}.tar.gz > ${pname}_${pversion}_download.log 2>&1
            $XVFBRUN_PATH -n $RANDOM $RPATH CMD check --no-manual --no-vignettes --no-build-vignettes -l "$R_LIBS_USER" ${pname}_${pversion}.tar.gz  > /dev/null 2>&1
            if [[ $? == 0 ]]; then
                touch ${pname}.CHECKOK
                printRightGreen "`date '+%H:%M:%S'` : OK"
            else
                touch ${pname}.CHECKPROBLEM
                printRightRed "`date '+%H:%M:%S'` : FAILED"
            fi

            if [[ $HTMLFLAG == 1 ]]; then
                generate_html_report $DESTINATION
            fi
        done < $PACKAGES_TO_CHECK_LIST_FILE
    else
        # TAR_FOLDER_PATH
        for i in $TAR_FOLDER_PATH/*; do
            file=`basename $i`
            pname=`echo $file | cut -d '_' -f 1`
            pversion=`echo $file | cut -d '_' -f 2`
            printLeftPurple "R CMD check ${pname}_${pversion} ... started at `date '+%H:%M:%S'` ... "

            $XVFBRUN_PATH -n $RANDOM $RPATH CMD check --no-manual --no-vignettes --no-build-vignettes -l "$R_LIBS_USER" $file  > /dev/null 2>&1
            if [[ $? == 0 ]]; then
                touch ${pname}.CHECKOK
                printRightGreen "`date '+%H:%M:%S'` : OK"
            else
                touch ${pname}.CHECKPROBLEM
                printRightRed "`date '+%H:%M:%S'` : FAILED"
            fi

            if [[ $HTMLFLAG == 1 ]]; then
                generate_html_report $DESTINATION
            fi
        done
    fi
else
    # MULTICORE PROCESSING

    # FUNCTION DEFINITION

    # how many packages are still not processed
    function nbToCheck(){
        result=0
        for i in ${status[@]}; do
            if [[ "$i" == "tocheck" ]]; then
                let "result = $result + 1"
            fi
        done
        echo $result
    }

    # how many packages are already processed (finished)
    function nbDone(){
        result=0
        for i in ${status[@]}; do
            if [[ "$i" == "done" ]]; then
                let "result = $result + 1"
            fi
        done
        echo $result
    }

    # which packages were not processed
    function whatsLeft(){
        let "end = $NB_PKG - 1"
        echo -ne "TO DO : "
        for zz in `seq 0 $end`; do
            if [[ "${status[$zz]}" != "done" ]]; then
                echo -ne "${names[$zz]} (${status[$zz]}) ; "
            fi
        done
        echo
    }

    # get index of first not-processed package in status list
    function getFirstToCheck(){
        result=0
        for i in ${status[@]}; do
            if [[ "$i" == "tocheck" ]]; then
                echo $result
                return
            fi
            let "result = $result + 1"
        done
    }

    # is the package $1 running (processus in activity)
    function isRunning(){
        slot=$1
        pid=${p_pid[$slot]}
        pname=${p_pkgname[$slot]}
        if [[ "`ps u -p $pid | grep $pname | grep 'R CMD check' | wc -l`" == 0 ]]; then
            echo "false"
        else
            echo "true"
        fi
    }

    # INITIALISATION of variables
    i=0
    if [ -n "$PACKAGE_TO_TEST" ] || [ -n "$CHECKLIST_FILE" ]; then
        while read line; do
            pname=`echo $line | cut -d ',' -f 1`
            pversion=`echo $line | cut -d ',' -f 2`
            names[$i]=$pname
            versions[$i]=$pversion
            status[$i]="tocheck"
            let "i = $i + 1"
        done < $PACKAGES_TO_CHECK_LIST_FILE
    else
        # TAR_FOLDER_PATH
        for j in $TAR_FOLDER_PATH/*; do
            file=`basename $j`
            pname=`echo $file | cut -d '_' -f 1`
            pversion=`echo $file | cut -d '_' -f 2`
            names[$i]=$pname
            versions[$i]=$pversion
            files[$i]=$file
            status[$i]="tocheck"
            let "i = $i + 1"
        done
    fi
    NB_PKG=$i
    echo "Number of packages : $NB_PKG"
    echo
    for i in `seq 1 $NBCORES`; do
        p_num[$i]=0
        p_pid[$i]=0
        p_pkgname[$i]=0
        p_pkgversion[$i]=0
        p_pkgfile[$i]=0
    done

    ## CHECK LOOP

    # while there is still a package to check or running
    while [[ "`nbDone`" != "$NB_PKG" ]]; do
        # for all slots
        for j in `seq 1 $NBCORES`; do
            # if the slot is empty OR not running anymore (no ps AND status==running)
            if [[ "${p_pkgname[$j]}" == "0" ]] ||
                    [[ "`isRunning $j`" == "false" &&
                    ${status[${p_num[$j]}]} == "running" ]]; then

                # if the slot is not empty AND the process is not running anymore
                if [[ "${p_pkgname[$j]}" != "0" ]] && [[ "`isRunning $j`" == "false" ]]; then
                    status[${p_num[$j]}]="done"
                    # display counter
                    nbdone=`nbDone`
                    let "pc = $nbdone * 100 / $NB_PKG"
                    #echo -ne "[$nbdone/$NB_PKG] [$pc %] "
                    printLeftPurple "R CMD check ${p_pkgname[$j]}_${p_pkgversion[$j]}.tar.gz on slot $j ..."
                    # if there was no error
                    #if [[ "`cat ${p_pkgname[$j]}.Rcheck/00check.log | egrep -i 'error|failed' | grep -v "OK" | grep -v "WARNING" | wc -l`" == "0" ]]; then
                    pid=${p_pid[$j]}
                    wait $pid
                    if [[ $? == 0 ]]; then
                        touch ${p_pkgname[$j]}.CHECKOK
                        #echo "R CMD check ${p_pkgname[$j]}_${p_pkgversion[$j]}.tar.gz ... FINISHED SUCCESSFULLY on slot $j at `date '+%H:%M:%S'` ... "
                        printRightGreen "FINISHED SUCCESSFULLY at `date '+%H:%M:%S'` [$nbdone/$NB_PKG] [$pc%]"
                    else
                        touch ${p_pkgname[$j]}.CHECKPROBLEM
                        #echo "R CMD check ${p_pkgname[$j]}_${p_pkgversion[$j]}.tar.gz ... FINISHED WITH ERROR on slot $j at `date '+%H:%M:%S'` ... "
                        printRightRed "FINISHED WITH ERROR at `date '+%H:%M:%S'` [$nbdone/$NB_PKG] [$pc%]"
                    fi
                fi

                # we start the next package check if there is still one to do
                if [[ "`nbToCheck`" != "0" ]]; then
                    ftc=`getFirstToCheck`
                    status[$ftc]="running"
                    pname=${names[$ftc]}
                    pversion=${versions[$ftc]}

                    mkdir $DESTINATION/${pname}.rlib
                    if [ -n "$PACKAGE_TO_TEST" ] || [ -n "$CHECKLIST_FILE" ]; then
                        echo "R CMD check ${pname}_${pversion}.tar.gz ... STARTED on slot $j at `date '+%H:%M:%S'` ... "

                        wget $DEFAULT_REPOSITORY/src/contrib/${pname}_${pversion}.tar.gz > ${pname}_${pversion}_download.log 2>&1
                        $XVFBRUN_PATH -n $RANDOM $RPATH CMD check --no-manual --no-vignettes --no-build-vignettes -l "$DESTINATION/${pname}.rlib" ${pname}_${pversion}.tar.gz  > /dev/null 2>&1 &
                        p_pid[$j]=$!
                    else
                        pfile=${files[$ftc]}
                        echo "R CMD check $pfile ... STARTED on slot $j at `date '+%H:%M:%S'` ... "
                        echo "$XVFBRUN_PATH -n $RANDOM $RPATH CMD check --no-manual --no-vignettes --no-build-vignettes -l \"$DESTINATION/${pname}.rlib\" $pfile  > /dev/null 2>&1 &"
                        $XVFBRUN_PATH -n $RANDOM $RPATH CMD check --no-manual --no-vignettes --no-build-vignettes -l "$DESTINATION/${pname}.rlib" $pfile  > /dev/null 2>&1 &
                        p_pid[$j]=$!
                        p_pkgfile[$j]=$pfile
                    fi
                    p_num[$j]=$ftc
                    p_pkgname[$j]=$pname
                    p_pkgversion[$j]=$pversion
                else
                    echo "SLOT $j has nothing left to do"
                fi
            fi
        done
        sleep 5
        if [[ $HTMLFLAG == 1 ]]; then
            generate_html_report $DESTINATION
        fi
    done
fi

# Result mail
if [[ $MAILTOFLAG == 1 ]]; then
    echo "Check results are here : 

$RESULT_URL

Your test directory is named `basename $DESTINATION`
" | mail -s "$MAIL_SUBJECT_BEGIN R check DONE (`basename $DESTINATION`)" $MAILTO
fi
